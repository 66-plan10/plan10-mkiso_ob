#!@BINDIR@/bash
# Copyright (c) 2015-2018 Eric Vidal <eric@obarun.org>
# All rights reserved.
# 
# This file is part of Obarun. It is subject to the license terms in
# the LICENSE file found in the top-level directory of this
# distribution.
# This file may not be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
#
# functions file for obarun-mkiso package

## 		Some global variables needed

HOME_PATH="/var/lib/obarun/obarun-mkiso"
MAKE_ISO="/usr/lib/obarun/mkiso/make.sh"
BUILD_ISO="/usr/lib/obarun/mkiso/build.sh"
WORK_DIR="${HOME_PATH}/work"
ARCH=$(uname -m)
PKG_LIST=""


clean_install(){
	
	oblog "Cleaning up"
	if [[ $(mount | grep "$NEWROOT"/proc) ]]; then
		oblog -t "Umount $NEWROOT"
		mount_umount "$NEWROOT" "umount"
	fi
	if [[ $(mount | grep "$WORK_DIR/airootfs/proc") ]]; then
		oblog -t "Umount $WORK_DIR/airootfs"
		mount_umount "$WORK_DIR/airootfs" "umount"
	fi
	if [[ $(awk -F':' '{ print $1}' /etc/passwd | grep usertmp) >/dev/null ]]; then
		oblog -t "Removing user usertmp"
		user_del "usertmp" &>/dev/null
	fi
	
	oblog "Restore your shell options"
	shellopts_restore

	exit
}

define_iso_variable(){
	local msg variable set
	msg="$1"
	variable="$2"
	
	oblog -i "Enter the $msg"
	read -e set
	
	while [[ -z $set ]]; do
		oblog -Wi "Empty value, please retry"
		read set
	done
	
	case $variable in
		ISO_NAME)
			ISO_NAME="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,ISO_NAME=.*$,ISO_NAME=\"${set}\",g" /etc/obarun/mkiso.conf;;
		ISO_VERSION)
			ISO_VERSION="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,ISO_VERSION=.*$,ISO_VERSION=\"${set}\",g" /etc/obarun/mkiso.conf;;
		ISO_LABEL)
			ISO_LABEL="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,ISO_LABEL=.*$,ISO_LABEL=\"${set}\",g" /etc/obarun/mkiso.conf;;
		ISO_PUBLISHER)
			ISO_PUBLISHER="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,ISO_PUBLISHER=.*$,ISO_PUBLISHER=\"${set}\",g" /etc/obarun/mkiso.conf;;
		ISO_APPLICATION)
			ISO_APPLICATION="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,ISO_APPLICATION=.*$,ISO_APPLICATION=\"${set}\",g" /etc/obarun/mkiso.conf;;
		INSTALL_DIR)
			INSTALL_DIR="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,INSTALL_DIR=.*$,INSTALL_DIR=\"${set}\",g" /etc/obarun/mkiso.conf;;
		OUT_DIR)
			OUT_DIR="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,OUT_DIR=.*$,OUT_DIR=\"${set}\",g" /etc/obarun/mkiso.conf;;
		IMAGE_MODE)
			while [[ $set != @(img|sfs) ]]; do
				oblog -W "sfs_mode must be img or sfs, please retry"
				read set
			done
			IMAGE_MODE="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,IMAGE_MODE=.*$,IMAGE_MODE=\"${set}\",g" /etc/obarun/mkiso.conf;;
		SFS_COMP)
			while [[ $set != @(gzip|lzma|lzo|xz) ]]; do
				oblog -W "sfs_comp must be gzip or lzma or lzo or xz, please retry"
				read set
			done
			SFS_COMP="${set}"
			oblog "${msg} is now : $set"
			sed -i "s,SFS_COMP=.*$,SFS_COMP=\"${set}\",g" /etc/obarun/mkiso.conf;;
		VERBOSE)
			reply_answer
			if (( ! $? )); then
				VERBOSE="yes"
				oblog "Verbose enabled"
				sed -i "s,VERBOSE=.*$,VERBOSE=\"yes\",g" /etc/obarun/mkiso.conf
			else
				VERBOSE="no"
				oblog "Verbose disabled"
				sed -i "s,VERBOSE=.*$,VERBOSE=\"no\",g" /etc/obarun/mkiso.conf
			fi;;
	esac
}

start_build(){
		
	check_mountpoint "$NEWROOT"
	
	if (( $? )); then
		die "$NEWROOT is not a valid mountpoint" "clean_install"
	fi
	
	${BUILD_ISO}
}

clean_work_dir(){
	if [[ -d $WORK_DIR ]]; then
		oblog -t "Removing $WORK_DIR"
		rm -R "$WORK_DIR" || die "unable to remove $WORK_DIR" "clean_install"
	else
		oblog -t "$WORK_DIR doesn't exist"
	fi
}

## 		Select root directory

choose_rootdir(){	
	local _directory
		
	oblog "Enter your root directory :"
	read -e _directory
		
	until [[ -d "$_directory" ]]; do
		oblog -W "This is not a directory, please retry :"
		read -e _directory
	done
	
	while ! mountpoint -q "$_directory"; do
		oblog -W "This is not a valide mountpoint, please retry :"
		read -e _directory
	done
	if [[ "${_directory}" == "/" ]];then
		oblog -W "Taking / as directory is not allowed"
		oblog -W "Keeping the old value"
		return
	fi
	oblog "Your root directory for installation is now : $_directory"
	#NEWROOT="${_directory}"
	sed -i "s,NEWROOT=.*$,NEWROOT=\"${_directory}\",g" /etc/obarun/mkiso.conf
	
	unset _directory
}

main_menu(){
	
	local step=100

while [[ "$step" !=  0 ]]; do
	source /etc/obarun/mkiso.conf
	clear
	oblog -i ""
	oblog -i ""
	oblog -i "**************************************************************"
	oblog -i "                       Iso menu"
	oblog -i "**************************************************************"
	oblog -i ""
	oblog -i " 1  -  Choose directory to copy on iso %g[$NEWROOT]%n"
	oblog -i " 2  -  Set iso name %g[$ISO_NAME]%n"
	oblog -i " 3  -  Set iso version %g[$ISO_VERSION]%n"
	oblog -i " 4  -  Set iso label %g[$ISO_LABEL]%n"
	oblog -i " 5  -  Set iso publisher %g[$ISO_PUBLISHER]%n"
	oblog -i " 6  -  Set application name for the iso %g[$ISO_APPLICATION]%n"
	oblog -i " 7  -  Set installation directory inside iso %g[$INSTALL_DIR]%n"
	oblog -i " 8  -  Set directory where the iso is saved %g[$OUT_DIR]%n"
	oblog -i " 9  -  Set SquashFS image mode (img or sfs) %g[$IMAGE_MODE]%n"
	oblog -i " 10 -  Set SquashFS compression type (gzip, lzma, lzo, xz) %g[$SFS_COMP]%n"
	oblog -i ""
	oblog -i " 11 -  Start building"
	oblog -i ""
	oblog -i "**************************************************************"
	oblog -i "                      Expert mode"
	oblog -i "**************************************************************"
	oblog -i ""
	oblog -i " 12 -  Enable verbose %g[$VERBOSE]%n"
	oblog -i " 13 -  Clean the working directory %g[$WORK_DIR]%n"
	oblog -i ""
	oblog -i ""
	oblog -i " %r0%n  -  Exit from mkiso script"
	oblog -i ""
	oblog -i ""
	oblog -i " Enter your choice :";read  step

		case "$step" in 
			1)	choose_rootdir;;
			2)	define_iso_variable "iso name" "ISO_NAME";; 
			3)	define_iso_variable "iso version" "ISO_VERSION";; 
			4)	define_iso_variable "iso label" "ISO_LABEL";; 
			5)	define_iso_variable "iso publisher" "ISO_PUBLISHER";; 
			6)	define_iso_variable "application name" "ISO_APPLICATION";;
			7)	define_iso_variable "installation directory" "INSTALL_DIR";; 
			8)	define_iso_variable "output directory" "OUT_DIR";; 
			9)	define_iso_variable "image mode [img|sfs]" "IMAGE_MODE";; 
			10)	define_iso_variable "compression type [gzip|lzma|lzo|xz]" "SFS_COMP";; 
			11)	oblog "Start building iso"
				start_build
				exit;;
			12) define_iso_variable "option for verbosity [y|n]" "VERBOSE";;	
			13) clean_work_dir;;
			0)	exit;;
			*) oblog -Wi "Invalid number, please retry:"
		esac
		oblog -ni "Press enter to return to the iso menu"
		read enter 
done
}

